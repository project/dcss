<?php

/**
 * @file Enables fully clearing the cache and registry
 */

/**
 * Implementation of hook_drush_command().
 */
function dcss_drush_command() {
  $items = array();

  $items['dcss-reset'] = array(
    'description' => 'Fully clear the cache, including theme settings and registry',
    'arguments' => array(
      'all' => 'Use this argument if all theme settings should be reset to defaults, otherwise omit it to reset just dcss settings.'
    ),
    'examples' => array(
      'drush dcss-reset' => 'Resets all dcss theme settings to their default values.',
      'drush dcss-reset all' => 'Resets all theme settings to their default values, not just dcss variables.',
    ),
  );

  return $items;
}

/**
 * Implementation of hook_drush_help().
 */
function dcss_drush_help($section) {
  switch ($section) {
    case 'drush:dcss-reset':
      $message = "The default cache clearing ability included in drush does not completely clear\n" .
        "the theme registry.  Since theme variables are stored, this can make it\n" .
        "difficult to test the changing of theme variables.  Use this function to ensure\n" .
        "values are reset to their defaults.";
      return dt($message);
      break;
  }
}

/**
 * Drush callback function to fully rebuild the cache
 */
function drush_dcss_reset($all = FALSE) {
  $message = dcss_reset_vars($all);
  drush_print(dt($message));
}

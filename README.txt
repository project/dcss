= Dynamic CSS =

Enables a themer to generate a CSS file while using PHP.

This module will go through all CSS files and replaces any file ending in '.php'
with a generated static CSS file that is uniquely identified by its last change
date and the dcss_vars() values.  This allows the dynamic CSS file to be
aggregated as normal.

When generating dynamic CSS you always have to ensure the uniqueness of the
dynamically generated file.  Always preset your variables through either your
theme's .info file or by invoking dcss_set().

A 'dcss' variable is included separate from the
'theme_<your-theme>_settings' variable to improve the 'everything-in-code'
development method.  Whenever you want your users to override variables, store
the values in 'dcss' so you can continue to deploy updates to the default
settings through your theme's .info file without reverting their overrides.

Due to the way Drupal handles a theme's settings, you must run the command
'drush dcss reset' after updating any dcss default variables and deploying new
code in order to ensure your new default theme settings are updated properly.


== Example ==

Add the dynamic CSS file to your theme just like a normal CSS file:

```
  stylesheets[all][]   = css/my-custom-styles.css.php
```

Your stylesheet can now contain PHP code:

```
  body {
    background-color: <?php print 'red'; ?>;
  }
```

Set default dynamic CSS variables in your theme's .info file:

```
  settings[dcss][background-color] = red
  settings[dcss][serif] = Georgia, Utopia, Palatino, serif
  settings[dcss][san-serif] = Helvetica, Arial, Tahoma, sans-serif
```

Recall dynamic CSS variables utilizing dcss():

```
  body {
    background-color: <?php print dcss('background-color'); ?>;
    font-family: <?php print dcss('san-serif'); ?>;
  }

  h1, h2, h3 {
    font-family: <?php print dcss('serif'); ?>;
  }
```

Any settings that are modified in a theme's .info file will not be made
immediately available as the default setting.  In order make them immediately
available, execute the following drush command:

```
  drush dcss reset
```


== Themer API ==

: dcss($var)
  Get the value of a variable
dcss_url($orig_url, $this_theme)
  Return the path of the original theme folder (since sites/default/files/dcss
  is generated, but assets need to be referenced from their original folders)
dcss_calc($val1, $val2, $operator)
  Performs math on two values (e.g. will remove suffix, perform math, then
  reapply suffix)
dcss_color($color, $behavior='0', $depth='bg', $default=NULL, $relationship=NULL, $relationship_phase=1)
  Allows you to alter a color by shade (behavior) or color relationship. If depth
  is set (fg or bg), it will make sure that a forground color is always visible.
dcss_opposite($value)
  Returns the opposite value of whatever is passed in (left, right, top, bottom)
dcss_group($selectors, $prefix=NULL, $suffix=NULL, $postprefix=NULL, $postsuffix=NULL)
  If you define a group of selectors as a variable (e.g. #main .class, #main .classtwo),
  it will allow you to append/prepend values to each member of the group
  (e.g. #main before, :hover after)

function dcss_curve($curve)
  Apply cross-browser compatable border-radiuses
dcss_boxshadow($shadow)
  (TODO: Needs to be implemented)
dcss_gradient($start_color, $end_color, $direction='top', $gradient_type='linear')
  Apply cross-browser gradients (TODO: Implement gradient type)



: dcss_url($orig_url, $this_theme)
  Return the path of the original theme folder (since sites/default/files/dcss
  is generated, but assets need to be referenced from their original folders)

: dcss_calc($val1, $val2, $operator)
  Performs math on two values (e.g. will remove suffix, perform math, then
  reapply suffix)

: dcss_color($color, $behavior='0', $depth='bg', $default=NULL, $relationship=NULL, $relationship_phase=1)
  Allows you to alter a color by shade (behavior) or color relationship. If
  depth is set (fg or bg), it will make sure that a forground color is always
  visible.

: dcss_opposite($value)
  Returns the opposite value of whatever is passed in (left, right, top, bottom)

: dcss_group($selectors, $prefix=NULL, $suffix=NULL, $postprefix=NULL, $postsuffix=NULL)
  If you define a group of selectors as a variable (e.g. #main .class, #main),
  it will allow you to append/prepend values to each member of the group
  (e.g. #main before, :hover after)

: function dcss_curve($curve)
  Apply cross-browser compatable border-radiuses

: dcss_boxshadow($shadow)
  (TODO: Needs to be implemented)

: dcss_gradient($start_color, $end_color, $direction='top', $gradient_type='linear')
  Apply cross-browser gradients (TODO: Implement gradient type)


== API ==

: dcss_make_file($file_path, $directory_path, $css_variables = array())
  Generates a rendered static CSS file and saves it in the directory provided

: dcss_set($var, $value)
  Set an override for a variable

: dcss_reset_vars($all = FALSE)
  Resets the dcss-related theme settings to their default values as recorded in
  each theme's .info files.

: dcss_vars()
  Returns all dynamic CSS variables and their values


== Hooks Implemented ==

: hook_theme_registry_alter()
  Force dcss_preprocess_page() to be processed last

: template_preprocess_page()
  Replaces all CSS files with a '.php' extension with a rendered static CSS file


== Additional Behaviors ==

=== Drush ===
: drush dcss reset
  Resets any 'dcss' overrides in your theme settings to the default values
  recorded in your theme's .info file.  This is required in order for any
  modifications to your default variables to be registered.


== Acknowledgments ==

: Style Settings
  http://www.drupal.org/project/style_settings

: CSS API
  http://www.drupal.org/project/cssapi

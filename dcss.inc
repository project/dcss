<?php

/**
 * @file Provides additional helper functions for themers
 */

/**
 * Take original file url from css file and link to it from the newly
 * generated "dcss" .css file that is now located in sites/default/files/dcss/
 *
 * Themer will have to specify the current theme your file is
 * in when you use dcss_url.
 *
 * Example in myfile.css.php:
 * background-image:url(<?php print dcss_url('../images/my_image.png', current_theme'); ?>;
 * will output in myfile.css
 * background-image:url(../../../all/themes/current_theme/images/my_image.png);
 *
 * @param string $orig_url - relative url as it would have been referenced to normally
 * @param string $this_theme - the theme containing the /images/ directory that
 * @param string $default - if you use default, it will use the directory provided by drupal
 * assuming you've used the upload.module
 * you wish to reference
 *
 * Example Use:
 * background-image:url(<?php print dcss_url('../images/myimage.png', 'my_theme_name'); ?>;
 *
 */
function dcss_url($orig_url, $this_theme, $default=NULL) {
  if ($default) {
    return base_path() . $orig_url;
  }
  $path = drupal_get_path('theme', $this_theme) . str_replace('..', '', $orig_url);
  $url = (url($path) == $path) ? $path : (base_path() . $path);
  return $url;
}

/**
 *  Perform Math on Two Variables
 *
 * @param string $val1 = value one
 * @param string $val2 = value two
 * @param string $operator = 'add', 'subtract', 'multiply', or 'divide' the
 * preceeding values
 */
function dcss_calc($val1, $val2, $operator) {
  preg_match('/([\d\.]+)(.+)/', $val1, $matches1);
  preg_match('/([\d\.]+)(.+)/', $val2, $matches2);
  if ($matches1[2] !== $matches2[2]) {
    // Mismatching suffixes. Hmm.
    return;
  }
  $val1_val = $matches1[1];
  $val2_val = $matches2[1];

  $percentages = $matches1[2] === '%';

  if ($percentages) {
    $val1_val /= 100;
    $val2_val /= 100;
  }

  switch ($operator) {
    case 'add':
    case '+':
      $total = $val1_val + $val2_val;
      break;
    case 'subtract':
    case '-':
      $total = $val1_val - $val2_val;
      break;
    case 'multiply':
    case '*':
      $total = $val1_val * $val2_val;
      break;
    case 'divide':
    case '/':
      $total = $val1_val / $val2_val;
      break;
    default:
      // Whaaa?
      return;
  }

  if ($percentages) {
    $total *= 100;
  }

  return $total . $matches1[2];
}

/**
 * Curved Corners
 *
 * @param string $curve - the border-radius according to the w3c spec e.g.:
 * <?php print dcss_curve('1px'); ?> // Applies to all corners
 * <?php print dcss_curve('1px 0'); ?> // TL=1px TR=0 BR=1px BL=0
 * <?php print dcss_curve('1px 0 2px'); ?> // TL=1px TR=0 BR=2px BL=0
 * <?php print dcss_curve('1px 0 2px 3px'); ?> // TL=1px TR=0 BR=2px BL=3px
 * dcss_curve('5px !important'); can be applied if you have specificity nightmares.
 *
 * Append new -vendor- prefixes where necessary
 *
 * TODO: Find IE solution:
 * Candidates:
 * http://code.google.com/p/curved-corner/ - Result: too many divs
 * HTC behavior fix: Fail
 * dd_roundies - can't do unique border radii, text child anti-alias is lost
 *
 * Try this next:
 * http://ragamo.medioclick.com/jquery/corners/ <-- Canvas rounded corners
 *
 */
function dcss_curve($curve, $important=NULL) {
  $curve_corner = explode(' ', trim($curve));
  $curve_count  = count($curve_corner);
  if ($important) {
    $imporant = ' !important';
  }

  $dcss_plugin_dir = '/'. $base_path . drupal_get_path('module', 'dcss') .'/plugins/';

  // if One Curve value, apply to all corners
  if ($curve_count == 1) {
    $tl = $curve_corner[0];
    $tr = $curve_corner[1];
    $br = $curve_corner[2];
    $bl = $curve_corner[3];

    return '-moz-border-radius:'. $tl . $important .';
        -webkit-border-radius:'. $tl . $important .';
        -webkit-background-clip:padding-box;
        -khtml-border-radius:'. $tl . $important .';
        border-radius:'. $tl . $important .';
    ';
  }
  elseif ($curve_count > 1) {
    // BR and BL repeated
    if ($curve_count == 2) {
      $tl = $curve_corner[0];
      $tr = $curve_corner[1];
      $br = $tl;
      $bl = $tr;
    }
    // BL repeats
    if ($curve_count == 3) {
      $tl = $curve_corner[0];
      $tr = $curve_corner[1];
      $br = $curve_corner[2];
      $bl = $tr;
    }
    // Four Unique Corners
    if ($curve_count == 4) {
      $tl = $curve_corner[0];
      $tr = $curve_corner[1];
      $br = $curve_corner[2];
      $bl = $curve_corner[3];
    }

    return '-moz-border-radius:'. $tl .' '. $tr .' '. $br .' '. $bl . $important .';
      -webkit-border-top-left-radius:'. $tl . $important .';
      -webkit-border-top-right-radius:'. $tr . $important .';
      -webkit-border-bottom-right-radius:'. $br . $important .';
      -webkit-border-bottom-left-radius:'. $bl . $important .';
      -webkit-background-clip:padding-box;
      -khtml-border-radius:'. $tl .' '. $tr .' '. $br .' '. $bl . $important .';
      border-radius:'. $tl .' '. $tr .' '. $br .' '. $bl . $important .';
    ';
  }
}


/**
 * Opacity (Cross Browser)
 *
 * @param integer $a - Alpha (0-100) (e.g. 50)
 * @param string $important - e.g.'!important'
 * It should be noted that this method will make all child elements inherit
 * the chosen transparency. If you want a background color to be transparent,
 * but the child content to be opaque, use dcss_trans_bg() method.
 *
 */
function dcss_opacity($a, $important=NULL) {
  $a = trim($a);
  if (($a != 100) && ($a != 0)) {
    $not_ie_a = ($a / 100);
  }
  elseif ($a == 100) {
    $not_ie_a = '1.0';
  }
  elseif ($a == 0) {
    $not_ie_a = 0;
  }
  if ($important) {
    $imp = ' !important'; // space before !important is.. important!
  }

  return '
  -ms-filter:"progid:DXImageTransform.Microsoft.Alpha(opacity='. $a .')"'. $imp .';
  filter:alpha(opacity='. $a .')'. $imp .';
  -khtml-opacity:'. $not_ie_a . $imp .';
  -moz-opacity:'. $not_ie_a . $imp .';
  opacity:'. $not_ie_a . $imp .';
  ';
}


/**
 * Transparent background-colors (that don't affect child element opacity)
 * If you want general opacity that affects all children, use dcss_opacity()
 *
 * @param string $color - Pick a hex color (e.g. #ff0000)
 * @param integer $a - Alpha (1-99) (e.g. 50) (If you want to use 0 or 100,
 * you shouldn't be using this method at all.
 *
 * TODO: Add a method similar to this for cross-browser trans border colors?
 *
 */
function dcss_trans_bg($color, $a) {
  $iecolor = trim(strtoupper(substr($color, -6)));
  $color   = trim(strtolower($color));

  if (!($a >= 100) && !($a <= 0)) {

    $not_ie_a = ($a / 10);

    // IE needs a whole number for the #AARRGGBB format
    include_once('plugins/color_converter.class.php');
    $c = new colorConverter;
    $c->isHEX($color);
    $rgb = $c->HEX2RGB($color);

    return '
    background-color:'. $color .';
    background-color:rgba('. $rgb[0] .', '. $rgb[1] .', '. $rgb[2] .', .'. $not_ie_a .');
    filter:  progid:DXImageTransform.Microsoft.gradient(startColorStr=\'#'. $a . $iecolor .'\',EndColorStr=\'#'. $a . $iecolor .'\'); /* IE6,7 */
    -ms-filter:"progid:DXImageTransform.Microsoft.gradient(startColorStr=\'#'. $a . $iecolor .'\', EndColorStr=\'#'. $a . $iecolor .'\')"; /* IE8 */
    display:inline-block\9;
    ';

  }
  elseif ($a >= 100) {
    return 'background-color:'. $color .';
    ';
  }
  else {
    return 'background-color:transparent;
    ';
  }
}


/**
 *  Box-Shadow
 *  (IE Support experimental and probably a bad idea. The shadow method is
 *   clearly inferior to modern shadow techniques)
 */
function dcss_boxshadow($color='none', $shadow_offsets=NULL, $inset=NULL, $important=NULL) {
  if ($important) {
    $important = ' !important';
  }

  if (strtolower(trim($color)) == 'none') {
    return '-moz-box-shadow:none'. $important .';
    -webkit-box-shadow:none'. $important .';
    -khtml-box-shadow:none'. $important .';
    box-shadow:none'. $important .';
    ';
  }
  $color = trim($color);
  $box_shadow = explode(' ', trim($shadow_offsets));

  if ($inset) {
    $inset = 'inset ';
  }

  $ho = trim($box_shadow[0]);
  $vo = trim($box_shadow[1]);
  $br = trim($box_shadow[2]);
  $brie = substr($br, 0, -2); /* TODO: Test for length of suffix and remove */

  /* TODO: Someday add IE boxshadow version when you figure out a safe method
    filter:
        progid:DXImageTransform.Microsoft.Shadow(color=#eeeeee,direction=0,strength=7)
        progid:DXImageTransform.Microsoft.Shadow(color=#dddddd,direction=90,strength=10)
        progid:DXImageTransform.Microsoft.Shadow(color=#dddddd,direction=180,strength=10)
        progid:DXImageTransform.Microsoft.Shadow(color=#eeeeee,direction=270,strength=7);

    using this next method requires a div to be positioned behind the current element..
    filter: progid:DXImageTransform.Microsoft.Blur(pixelRadius=4,MakeShadow=true,ShadowOpacity=0.35); background: #000000;

  */

  $output = '-moz-box-shadow:'. $inset . $ho .' '. $vo .' '. $br .' '. $color .''. $important .';
  -webkit-box-shadow:'. $inset . $ho .' '. $vo .' '. $br .' '. $color .''. $important .';
  -khtml-box-shadow:'. $inset . $ho .' '. $vo .' '. $br .' '. $color .''. $important .';
  box-shadow:'. $inset . $ho .' '. $vo .' '. $br .' '. $color .''. $important .';
  ';

  return $output;
}

/**
 * @param string $start_color - Color referenced at the beginning of the gradient
 * @param string $end_color - Color referenced at the end of the gradient
 * @param string $direction - Direction origin of the gradient
 * @param string $gradient_type - style of gradient 'linear', TODO: Radial
 * TODO: Background-images seem to get overridden by this method.. need to find
 * a way to inherit them without using "inherit"
 *
 */
function dcss_gradient($start_color, $end_color, $direction='top', $gradient_type='linear') {
  switch ($direction) {
    case 'top':
      $grad_1 = $start_color;
      $grad_2 = $end_color;
    case 'bottom':
      $grad_1 = $start_color;
      $grad_2 = $end_color;
    default:
      $grad_1 = $start_color;
      $grad_2 = $end_color;
  }
  // TODO: determine what in plugins/csscolor.php is returning the shorthand of #FFFFFF to prevent this for IE transforms
  $sc = strtolower($start_color);
  $ec = strtolower($end_color);
  if ($sc == '#fff') {
    $start_color = '#FFFFFF';
  }
  if ($ec == '#fff') {
    $end_color = '#FFFFFF';
  }

  return '
  background-color:'. $start_color .';
  background:-webkit-gradient('. $gradient_type .', left '. $direction .', left '. dcss_opposite($direction) .', from('. $start_color .'), to('. $end_color .'));
  background:-moz-linear-gradient('. $direction .', '. $start_color .', '. $end_color .');
  filter:progid:DXImageTransform.Microsoft.gradient(startColorStr='. $start_color .', EndColorStr='. $end_color .');
  -ms-filter:"progid:DXImageTransform.Microsoft.gradient(startColorStr=\''. $start_color .'\', EndColorStr=\''. $end_color .'\')";
  display:inline-block\9;
  ';

}

/**
 *
 * Commonwealth Color Declarations:
 *
 * Process Colors supplied by .info file
 * If color needs to be output with different shade, use one of these methods
 * within a css.php file. Variables need to be reassigned here to be globally applied.
 *
 * background-color:<?php print dcss_color(dcss('color-x'), -2); ?>;
 *
 *
 * @param string $color - a dcss variable (hex color in this case) TODO: Needs to support
 * shorthand hex colors, RGB and RGBa.
 * @param string $behavior -
 * @param string $depth - allowed values: bg OR fg.
 * @param string $default - If a default color exists, use that instead of calculated colors
 * @param string $relationship - 'mono' (default), 'complement', 'triad', 'tetrad', 'analogic',
 * or color calculated then passed through the usual color methods
 * @param integer $relationship_phase - 0 (original color), 1 - one phase of relationship, 2 - two phases etc...
 * bg defines a gradient of the original colors (-5 shades and +5 shades)
 * fg defines the opposite, readable (by W3C minimum readable color (-5 shades and +5 shades)
 *
 * TODO: Set arguments for minimum-color difference (default is 500)
 * TODO: Set arguments for minimum brightness difference (126)
 */

function dcss_color($color, $behavior='0', $depth='bg', $default=NULL, $relationship=NULL, $relationship_phase=1) {

  // Color Relationships
  if ($relationship) {
    include('plugins/color_harmony.class.php');
    $c = new colorHarmony;
    $c->isHEX($color);

    $relationship = trim(strtolower($relationship));

    switch ($relationship) {
      case 'mono':
        $temp_color = $c->Monochromatic($color);
        break;
      case 'complement':
        $temp_color = $c->Complementary($color);
      break;
      case 'triad':
        $temp_color = $c->Triads($color);
        break;
      case 'tetrad':
        // TODO: Tetrad method
        break;
      case 'analogic':
        $temp_color = $c->Analogous($color);
      break;
      default:
        $temp_color = $c->Monochromatic($color);
      break;
    }
    // Apply Phase to Color
    $color = $temp_color[$relationship_phase];
  }


  // If Default Color Exists
  if ($default) {
    if ($depth == 'bg') {
      return $default;
    }
    else {
      $color = $default;
    }
  }

  // Include Color Class
  include_once("plugins/csscolor.php");

  $dcss_color = new CSS_Color(substr($color, -6));

  switch ($depth) {
    case 'bg':
      return '#'. $dcss_color->bg[$behavior];
    break;
    case 'fg':
      return '#'. $dcss_color->fg[$behavior];
    break;
  }
}

/**
 * TODO: CSS Sprites
 * Need to make it so if we add icons to the sprite sheet it is easily scalable
 */

function dcss_sprite($x, $y) {
  /* Define Aliases for Sprites
  ------------------------*/
  /*$sprite_top_offset = 0;
  $sprite_x_offset   = 0;
  $sprite_y_offset   = 0;

  $sprite_names = array();

  foreach ($sprite_names as $sprite_name) {
    $sprite_name
  }*/
  $bgp = 'background-position:'. $x .' '. $y .';';
}

/**
 * Quick function to give the opposite of something
 */
function dcss_opposite($value) {
  switch ($value) {
    // Left and Right
    case 'left':
      return 'right';
    case 'right':
      return 'left';
    case 'top':
      return 'bottom';
    case 'bottom':
      return 'top';
    case 'ltr':
      return 'rtl';
    case 'rtl':
      return 'ltr';

    // Just return the original value if we don't have an opposite system set
    // up for it
    default:
      return $value;
  }
}

/**
 * Use your dcss layout-direction variable to set the page.tpl.php language direction
 */
function dcss_language_dir($dir) {
  switch ($dir) {
    case 'left':
      return 'ltr';
    case 'right':
      return 'rtl';
  }
}

/**
 * A Method for declaring a large group of selectors and making it simple to
 * append new states (e.g. :hover, :active, etc...) to the groups. This will
 * prevent the need to manually update several groups of the selectors
 *
 * Declare groups of selectors in a theme's dcss include file (commonwealth.dcss.inc)
 * e.g $group_blocks = '#elementone, #elementtwo, .class-one';
 *
 * In your .css.php files, call the selectors like so:
 * <?php print dcss_group($group_blocks); ?> {...}
 *
 * If you want to add prefixes to each element:
 * <?php print dcss_group($group_blocks, '#main'); ?>
 * and it will output the selectors like so:
 * #main #elementone,
 * #main #elementtwo {...}
 *
 * If you want to add a suffix to the end of a group, do this:
 * (Note that there is no space added by default for $suffix. This is designed
 * to allow you to add .class.chaining or :pseudo states to groups)
 * <?php print dcss_group($group_blocks, NULL, ':hover'); ?>
 * Will output:
 * #elementone:hover,
 * #elementtwo:hover {...}
 *
 * TODO: For future improvement, it would be cool if we could generate one,
 * unique class, apply this group's properties to this class, determine what
 * elements have been selected in this group, and apply the new class to
 * their HTML elements. This would prevent code from bulking up while allowing
 * drupal themes to scale nicely.
 *
 * Heaven forbid that you need to use $postprefix or $postsuffix,
 * but it could be nice if you want to inject a class after your prefix to control
 * the child elements ($group_blocks) differently
 */
function dcss_group($selectors, $prefix=NULL, $suffix=NULL, $postprefix=NULL, $postsuffix=NULL) {
  if (!is_array($selectors)) {
    $selectors = explode(',', trim($selectors));
  }

  $newgroup = array();

  foreach ($selectors as $selector) {
    $newgroup[] = $prefix . $postprefix . $selector . $suffix . $postsuffix;
  }

  $newgroup = implode(',', $newgroup);

  return $newgroup;
}

/**
 * Hide Text with negative text-indent, even if you flip layout rtl
 * negative text-indent requires block level status and left text align.
 * I don't wish to add !important to these properties to avoid overriding child elements,
 * so I would advise you call this method after every other property
 * text-align: doesn't need to be changed, as changing the html dir="" param will
 * affect negative text indentation
 * text-decoration will draw huge ugly link lines, so it is best practice to remove them
 * apply this to the actual anchor when possible
 */
function dcss_hide_text() {
  $layoutdir = dcss('layout-direction');

  return 'display:block;
  text-align:'. $layoutdir .';
  text-decoration:none;
  text-indent:-9999em;
  ';
}

/**
 * Rotate
 * @param integer $degree - (e.g. 45, 15.2, -270.2) the angle of rotation you
 * want to spin a dom element.
 * IE needs radians to rotate but the conversion is done for you in this method.
 * Note that IE rotation doesn't rotate children elements reliably
 */
function dcss_rotate($degree) {

  $deg2radians = pi() * 2 / 360;
  $rad = $degree * $deg2radians;
  $costheta = cos($rad);
  $sintheta = sin($rad);

  $M11 = $costheta;
  $M12 = -$sintheta;
  $M21 = $sintheta;
  $M22 = $costheta;

  return '
  -moz-transform:rotate('. $degree .'deg);
  -webkit-transform:rotate('. $degree .'deg);
  -o-transform:rotate('. $degree .'deg);
  filter:progid:DXImageTransform.Microsoft.Matrix(sizingMethod="auto expand",
           M11='. $M11 .', M12='. $M12 .', M21='. $M21 .', M22='. $M22 .');
  -ms-filter:"progid:DXImageTransform.Microsoft.Matrix(SizingMethod="auto expand",
           M11='. $M11 .', M12='. $M12 .', M21='. $M21 .', M22='. $M22 .')";
  zoom:1;
  ';
}


/**
 * Return specific information about an imagecache preset
 *
 * @param String preset
 *   A preset name.
 * @param String Type
 *   what information to return.
 * @param reset
 *   if set to TRUE it will clear the preset cache
 *
 * @return
 *   array of presets array( $preset_id => array('presetid' => integer, 'presetname' => string))
 */
function dcss_imagecache($preset, $type = 'width', $reset = FALSE) {
  static $info = array();
  $dimensions = array('height', 'width');
  if (empty($info[$preset]) || !empty($reset)) {
    // this is cached data
    $presets = imagecache_presets();
    $info[$preset]['width'] = 0;
    $info[$preset]['height'] = 0;
    // go through each action
    foreach ($presets[$preset]['actions'] as $action) {
      // go through each dimension
      foreach ($dimensions as $dimension) {
        // if dimension is set, see if should add it
        if (!empty($action['data'][$dimension])) {
          // if is a percent, and there's already a dimension set, apply it
          if (strpos($action['data'][$dimension], '%') !== FALSE) {
            if (!empty($action['data'][$dimension])) {
              $info[$preset][$dimension] = _imagecache_percent_filter($action['data'][$dimension], $info[$preset][$dimension]) .'px';
            }
          }
          else {
            // set the preset
            $info[$preset][$dimension] = $action['data'][$dimension] .'px';
          }
        }
      }
    }
    $info[$preset]['both'] = $info[$preset]['width'] .'px '. $info[$preset]['height'] .'px';
  }
  if (!empty($type)) {
    return (!empty($info[$preset][$type])?$info[$preset][$type]:0);
  }
  else {
    return $info[$preset];
  }
}

/**
 * No Select
 * Prevents a user from selecting the text where this method is applied
 * It should be noted that your text will not be searchable (via browser's
 * "find" methods), so be very careful when using this!
 *
 * TODO: IE support, Opera Support
 */
function dcss_noselect() {
  return '-moz-user-select:none;
  -webkit-user-select:none;
  -khtml-user-select:none;
  ';
}

/**
 * Min-Height (Cross-Browser)
 */
function dcss_min_height($height) {
  return 'min-height:'. $height .';
  height:auto !important;
  height:'. $height .';
  ';
}

/**
 * Max-Height (Cross-Browser)
 */
function dcss_max_height($height) {
  /* TODO: IE throws "active content" warning when using the height expression
   * need to find work around for this:
   * height: expression( this.scrollHeight > 332 ? "333px" : "auto" );
   */
  return 'max-height:'. $height .';
  overflow:scroll;
  ';
}


/**
 * Min-Width (Cross-Browser)
 */
function dcss_min_width($width) {
  $intwidth = substr($width, -2);
  $output = 'width:'. $width .';
  _width:expression(document.body.clientWidth < '. $intwidth .'? "'. $width .'": "auto" );
  ';
  return $output;
}

function dcss_max_width($width) {
  $intwidth = substr($width, -2);
  $output = 'max-width:'. $width .';
  _width:expression(document.body.clientWidth > '. $intwidth .'? "'. $width .'" : "auto");
  ';
  return $output;
}

/**
 * Word-wrap (Cross-Browser)
 * the white-space pre-wraps will prevent super-long strings from breaking layout
 * I would suggest you only add this to the lowest level child possible
 */
function dcss_word_wrap() {
  return 'white-space:-moz-pre-wrap;
  white-space:-hp-pre-wrap;
  white-space:-o-pre-wrap;
  white-space:-pre-wrap;
  white-space:pre-wrap;
  word-wrap:break-word;
  ';
}
